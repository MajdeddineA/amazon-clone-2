import firebase from "firebase";


const firebaseConfig = {
  apiKey: "AIzaSyDuuZOApiKoRrcB3BqKwd8KRsp9mgQ-nno",
  authDomain: "amzn-clone-2.firebaseapp.com",
  projectId: "amzn-clone-2",
  storageBucket: "amzn-clone-2.appspot.com",
  messagingSenderId: "281086025975",
  appId: "1:281086025975:web:e3be128c81c291668ab3f2"
};

  const app = !firebase.apps.length 
    ? firebase.initializeApp(firebaseConfig) 
    : firebase.app();

    const db = app.firestore();

    export default db;